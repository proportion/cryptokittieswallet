package ch.proportion.cryptokittieswallet.views.transactions;

import ch.proportion.cryptokittieswallet.data.entity.Transaction;
import ch.proportion.cryptokittieswallet.data.service.TransactionService;
import ch.proportion.cryptokittieswallet.views.MainLayout;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.vaadin.artur.helpers.CrudServiceDataProvider;

import java.time.format.DateTimeFormatter;
import java.util.Optional;

@PageTitle("Transactions")
@Route(value = "transactions/:transactionID?", layout = MainLayout.class)
public class TransactionsView extends Div implements BeforeEnterObserver {

  private final Grid<Transaction> transactionGrid;
  private final TransactionService transactionService;

  public TransactionsView(final TransactionService transactionService) {
    this.transactionService = transactionService;
    addClassNames("transactions-view", "flex", "flex-col", "h-full");
    final DateTimeFormatter swissFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    // Configure Grid
    transactionGrid = new Grid<>(Transaction.class, false);
    transactionGrid.addColumn("action");
    transactionGrid.addColumn("name");
    transactionGrid.addColumn(transaction -> transaction.getTimestamp().format(swissFormatter), "timestamp").setHeader("Timestamp");
    transactionGrid.setDataProvider(new CrudServiceDataProvider<>(transactionService));

    // when a row is selected or deselected, populate form
    transactionGrid.asSingleSelect().addValueChangeListener(event -> {
      if (event.getValue() != null) {
        UI.getCurrent().navigate(String.format("transactions/%d", event.getValue().getId()));
      } else {
        UI.getCurrent().navigate(TransactionsView.class);
      }
    });

    add(transactionGrid);
  }

  @Override
  public void beforeEnter(final BeforeEnterEvent event) {
    final Optional<Integer> transactionID = event.getRouteParameters().getInteger("transactionID");
    if (transactionID.isPresent()) {
      final Optional<Transaction> transactionFromBackend = transactionService.get(transactionID.get());
      if (transactionFromBackend.isPresent()) {
        transactionGrid.select(transactionFromBackend.get());
      } else {
        Notification.show(
                String.format("The requested Transaction was not found, ID = %d", transactionID.get()),
                3000, Notification.Position.TOP_CENTER);
        // when a row is selected but the data is no longer available,
        // refresh grid
        transactionGrid.select(null);
        transactionGrid.getDataProvider().refreshAll();
        event.forwardTo(TransactionsView.class);
      }
    }
  }
}