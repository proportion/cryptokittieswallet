package ch.proportion.cryptokittieswallet.views;

import ch.proportion.cryptokittieswallet.views.assets.AssetsView;
import ch.proportion.cryptokittieswallet.views.transactions.TransactionsView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Header;
import com.vaadin.flow.component.html.ListItem;
import com.vaadin.flow.component.html.Nav;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.html.UnorderedList;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;

import java.util.List;

/**
 * The main view is a top-level placeholder for other views.
 */
@PWA(name = "CryptoKittiesWallet", shortName = "CryptoKittiesWallet", enableInstallPrompt = false)
@Theme(themeFolder = "cryptokittieswallet", variant = Lumo.LIGHT)
@PageTitle("Main")
public class MainLayout extends AppLayout {

  public MainLayout() {
    Header header = new Header();
    header.addClassNames("bg-base", "border-b", "border-contrast-10", "box-border", "flex", "flex-col", "w-full");

    Div layout = new Div();
    layout.addClassNames("flex", "h-xl", "items-center", "px-l");

    H1 title = new H1("CryptoKittiesWallet");
    title.addClassNames("my-0", "me-auto", "text-l");
    layout.add(title);

    Nav navigation = new Nav();
    navigation.addClassNames("flex", "gap-s", "overflow-auto", "px-m");

    // Wrap the links in a list; improves accessibility
    UnorderedList navigationList = new UnorderedList();
    navigationList.addClassNames("flex", "list-none", "m-0", "p-0");
    navigation.add(navigationList);

    for (RouterLink link : newLinks()) {
      ListItem item = new ListItem(link);
      navigationList.add(item);
    }

    header.add(layout, navigation);
    addToNavbar(header);
  }

  private List<RouterLink> newLinks() {
    return List.of(
            newLink(new MenuItemInfo("Assets", "la la-image", AssetsView.class)),
            newLink(new MenuItemInfo("Transactions", "la la-th-list", TransactionsView.class))
    );
  }

  private static RouterLink newLink(final MenuItemInfo menuItemInfo) {
    RouterLink link = new RouterLink();
    link.addClassNames("flex", "h-m", "items-center", "px-s", "relative", "text-secondary");
    link.setRoute(menuItemInfo.getView());

    Span icon = new Span();
    icon.addClassNames("me-s", "text-l");
    if (!menuItemInfo.getIconClass().isEmpty()) {
      icon.addClassNames(menuItemInfo.getIconClass());
    }

    Span text = new Span(menuItemInfo.getText());
    text.addClassNames("font-medium", "text-s", "whitespace-nowrap");

    link.add(icon, text);
    return link;
  }

  public static class MenuItemInfo {

    private final String text;
    private final String iconClass;
    private final Class<? extends Component> view;

    public MenuItemInfo(final String text,
                        final String iconClass,
                        final Class<? extends Component> view) {
      this.text = text;
      this.iconClass = iconClass;
      this.view = view;
    }

    public String getText() {
      return text;
    }

    public String getIconClass() {
      return iconClass;
    }

    public Class<? extends Component> getView() {
      return view;
    }
  }
}