package ch.proportion.cryptokittieswallet.views.assets;

import ch.proportion.cryptokittieswallet.data.entity.Asset;
import ch.proportion.cryptokittieswallet.data.service.AssetService;
import ch.proportion.cryptokittieswallet.views.MainLayout;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@PageTitle("Assets")
@Route(value = "assets", layout = MainLayout.class)
@RouteAlias(value = "", layout = MainLayout.class)
public class AssetsView extends Div {

  private final FlexLayout cardsPane;

  public AssetsView(final AssetService assetService) {
    addClassName("assets-view");

    final HorizontalLayout titleLayout = new HorizontalLayout();
    final H3 title = new H3("Assets");
    titleLayout.add(title);
    titleLayout.setAlignItems(FlexComponent.Alignment.BASELINE);
    add(titleLayout);

    final Paragraph description = new Paragraph("Buy and sell NFT assets and grow an NFT empire");
    add(description);

    final Button buyButton = new Button("Buy");
    buyButton.setThemeName("primary");
    titleLayout.add(buyButton);

    cardsPane = new FlexLayout();
    cardsPane.setFlexWrap(FlexLayout.FlexWrap.WRAP);
    cardsPane.setWidth("100%");
    cardsPane.setFlexDirection(FlexLayout.FlexDirection.ROW);
    add(cardsPane);

    buyButton.addClickListener(e -> {
      final Asset asset = assetService.buyAsset();
      final AssetCard card = newAssetCard(assetService, asset);
      cardsPane.add(card);
    });

    final Page<Asset> list = assetService.listAssets(Pageable.unpaged());
    for (Asset asset : list) {
      final AssetCard card = newAssetCard(assetService,  asset);
      cardsPane.add(card);
    }
  }

  private AssetCard newAssetCard(final AssetService assetService,
                                 final Asset asset) {
    final AssetCard card = new AssetCard(asset);
    card.addSellListener(event -> {
      assetService.sellAsset(asset);
      cardsPane.remove(card);
    });
    return card;
  }
}