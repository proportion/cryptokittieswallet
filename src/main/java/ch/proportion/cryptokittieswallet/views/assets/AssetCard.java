package ch.proportion.cryptokittieswallet.views.assets;

import ch.proportion.cryptokittieswallet.data.entity.Asset;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

class AssetCard extends Div {

  private final Button sellButton;

  public AssetCard(final Asset asset) {
    addClassNames("asset-view", "bg-contrast-5", "p-m", "rounded-l");

    final Image image = new Image(asset.getImageURL(), asset.getName());
    final Span name = new Span(asset.getName());
    name.addClassNames("text-xl", "font-semibold");
    sellButton = new Button("Sell");

    final HorizontalLayout layout = new HorizontalLayout(name, sellButton);
    layout.setJustifyContentMode(FlexComponent.JustifyContentMode.EVENLY);

    add(image, layout);
  }

  public void addSellListener(final ComponentEventListener<ClickEvent<Button>> listener) {
    sellButton.addClickListener(listener);
  }
}