package ch.proportion.cryptokittieswallet.data.entity;

import javax.persistence.Entity;
import java.time.LocalDateTime;

@Entity
public class Transaction extends AbstractEntity {

  private String action;
  private String name;
  private LocalDateTime timestamp;

  public Transaction() {
  }

  public Transaction(final String name,
                     final String action,
                     final LocalDateTime timestamp) {
    this.action = action;
    this.name = name;
    this.timestamp = timestamp;
  }

  public String getAction() {
    return action;
  }

  public void setAction(final String action) {
    this.action = action;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public LocalDateTime getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(final LocalDateTime timestamp) {
    this.timestamp = timestamp;
  }
}