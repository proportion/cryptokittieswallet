package ch.proportion.cryptokittieswallet.data.entity;

import javax.persistence.Entity;

@Entity
public class Asset extends AbstractEntity {

  private String name;
  private String imageURL;

  public Asset() {
  }

  public Asset(final String name, final String imageURL) {
    this.name = name;
    this.imageURL = imageURL;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getImageURL() {
    return imageURL;
  }

  public void setImageURL(final String imageURL) {
    this.imageURL = imageURL;
  }
}