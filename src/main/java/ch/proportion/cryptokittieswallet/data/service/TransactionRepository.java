package ch.proportion.cryptokittieswallet.data.service;

import ch.proportion.cryptokittieswallet.data.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;

interface TransactionRepository extends JpaRepository<Transaction, Integer> {

}