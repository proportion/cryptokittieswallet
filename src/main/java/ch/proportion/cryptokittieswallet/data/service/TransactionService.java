package ch.proportion.cryptokittieswallet.data.service;

import ch.proportion.cryptokittieswallet.data.entity.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vaadin.artur.helpers.CrudService;

@Service
public class TransactionService extends CrudService<Transaction, Integer> {

  private final TransactionRepository repository;

  public TransactionService(@Autowired final TransactionRepository repository) {
    this.repository = repository;
  }

  @Override
  protected TransactionRepository getRepository() {
    return repository;
  }
}