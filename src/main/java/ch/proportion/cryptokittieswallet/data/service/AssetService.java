package ch.proportion.cryptokittieswallet.data.service;

import ch.proportion.cryptokittieswallet.data.entity.Asset;
import ch.proportion.cryptokittieswallet.data.entity.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

@Service
public class AssetService {

  private final AssetRepository assetRepository;
  private final TransactionRepository transactionRepository;

  private static final List<Asset> sampleAssets = List.of(
          new Asset("Brian", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/1280518.svg"),
          new Asset("Hela", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/229366.svg"),
          new Asset("Waldo", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/1280382.svg"),
          new Asset("Kitty", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/242892.svg"),
          new Asset("Paw", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/246360.svg"),
          new Asset("Sergej", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/247763.svg"),
          new Asset("Smarty", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/248096.svg"),
          new Asset("Luks", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/248127.svg"),
          new Asset("Richard", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/249002.svg"),
          new Asset("Chestnut", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/249320.svg"),
          new Asset("Vaffle", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/249711.svg"),
          new Asset("Goldie", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/249712.svg"),
          new Asset("Holly", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/1992683.png"),
          new Asset("Boris", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/1981851.png"),
          new Asset("Harriet", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/2005613.png"),
          new Asset("CyberBerry", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/282.png"),
          new Asset("Papacat", "https://img.cryptokitties.co/0x06012c8cf97bead5deae237070f9587f8e7a266d/1500000.png")
  );
  private static final Random RANDOM = new Random();

  public AssetService(@Autowired final AssetRepository assetRepository,
                      @Autowired final TransactionRepository transactionRepository) {
    this.assetRepository = assetRepository;
    this.transactionRepository = transactionRepository;
  }

  public Asset buyAsset() {
    final int randomKittyIndex = RANDOM.nextInt(sampleAssets.size());
    final Asset asset = assetRepository.save(new Asset(sampleAssets.get(randomKittyIndex).getName(), sampleAssets.get(randomKittyIndex).getImageURL()));
    transactionRepository.save(new Transaction(asset.getName(), "Buy", LocalDateTime.now()));
    return asset;
  }

  public void sellAsset(final Asset asset) {
    transactionRepository.save(new Transaction(asset.getName(), "Sell", LocalDateTime.now()));
    assetRepository.delete(asset);
  }

  public Page<Asset> listAssets(final Pageable pageable) {
    return assetRepository.findAll(pageable);
  }
}