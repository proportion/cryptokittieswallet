package ch.proportion.cryptokittieswallet.data.service;

import ch.proportion.cryptokittieswallet.data.entity.Asset;
import org.springframework.data.jpa.repository.JpaRepository;

interface AssetRepository extends JpaRepository<Asset, Integer> {

}